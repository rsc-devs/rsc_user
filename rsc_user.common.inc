<?php


/**
 * (re)create fields and instances
 *
 * @throws \FieldException
 */
function rsc_user_field_config() {

  $t = get_t(); // this function might run during install, or any other time
  $prefix = 'rscu'; // RSC user
  $fieldnames = array_keys(field_info_fields());

  // Try to create field for the user's full name and surname
  if (!in_array("{$prefix}_fullname", $fieldnames)) {
    field_create_field([
      'field_name' => "{$prefix}_fullname",
      'type' => 'text',
      'cardinality' => 1,
    ]);
  }

  // Try to create field for the user's country
  if (!in_array("{$prefix}_country", $fieldnames)) {
    field_create_field([
      'field_name' => "{$prefix}_country",
      'type' => 'text',
      'cardinality' => 1,
    ]);
  }

  // reload field names to see whether it worked
  $fieldnames = array_keys(field_info_fields());

  // also get the field instances attached to user entities
  $instances = array_keys(field_info_instances('user', 'user'));

  // Try to attach field instances to user entities

  if (!in_array("{$prefix}_fullname", $instances) && in_array("{$prefix}_fullname", $fieldnames)) {
    field_create_instance([
      'field_name' => "{$prefix}_fullname",
      'entity_type' => 'user',
      'bundle' => 'user',
      'label' => $t('Full name and surname'),
      'required' => FALSE,
      'settings' => [
        'user_register_form' => TRUE,
      ],
      'widget' => [
        'weight' => -20,  // at the top
      ],
      'description' => "Enter your full name and surname. This will be used as a display name for communications.",
    ]);
    $instances[] = "{$prefix}_fullname";
  }

  if (!in_array("{$prefix}_country", $instances) && in_array("{$prefix}_country", $fieldnames)) {
    field_create_instance([
      'field_name' => "{$prefix}_country",
      'entity_type' => 'user',
      'bundle' => 'user',
      'label' => $t('Country'),
      'required' => TRUE,
      'settings' => [
        'user_register_form' => TRUE,
      ],
      'widget' => [
        'weight' => 1,    // after email, before timezone
      ],
      'description' => "This information will be kept private.",
    ]);
    $instances[] = "{$prefix}_country";
  }

}


/**
 * Get user IDs of users with specific roles.
 * With help from https://drupal.stackexchange.com/a/11186/8452
 */
function get_user_ids_having_roles($rids) {
  $result = db_query(
    'SELECT DISTINCT(ur.uid) FROM {users_roles} AS ur WHERE ur.rid IN (:rids)',
    [':rids' => $rids]
  );
  return $result->fetchCol();
}


/**
 * Get the full name of the user, and fall back to the short username if the
 * full name is not specified.
 */
function get_user_display_name($user) {
  $user_wrapper = entity_metadata_wrapper('user', $user);
  $rscu_fullname = $user_wrapper->rscu_fullname->value();
  if (!empty($rscu_fullname)) {
    return check_plain(trim($rscu_fullname));
  }
  return format_username($user);
}
